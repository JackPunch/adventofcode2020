import pytest
import day2


@pytest.mark.parametrize(
    "strInput, output", [(["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"], 2)]
)
def test_part1(strInput, output):
    formatted_input = day2.format_input(strInput)
    ans = day2.part1(formatted_input)
    assert ans == output


@pytest.mark.parametrize(
    "strInput, output", [(["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"], 1)]
)
def test_part2(strInput, output):
    formatted_input = day2.format_input(strInput)
    ans = day2.part2(formatted_input)
    assert ans == output
