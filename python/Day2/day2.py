import os
import regex as re


class PasswordClass:
    def __init__(self, password, letter, min_count, max_count):
        self.password = password
        self.letter = letter
        self.min_count = min_count
        self.max_count = max_count


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        lines = f.readlines()

    return lines


def format_input(lines):
    password_conditions = []
    pattern = r"\b([1-9][0-9]?)+-([1-9][0-9]?)+\s+([a-z])+:+\s+(\w+)"
    for line in lines:
        rx_match = re.match(pattern, line)
        password_conditions.append(
            PasswordClass(
                password=rx_match.group(4),
                letter=rx_match.group(3),
                max_count=int(rx_match.group(2)),
                min_count=int(rx_match.group(1)),
            )
        )
    return password_conditions


def part1(all_passwords):
    count = 0
    for password in all_passwords:
        letter_count = password.password.count(password.letter)
        if password.min_count <= letter_count <= password.max_count:
            count += 1
    print(count)
    return count


def part2(all_passwords):
    count = 0
    for password in all_passwords:
        if (password.password[password.min_count - 1] == password.letter) ^ (
            password.password[password.max_count - 1] == password.letter
        ):
            count += 1
    print(count)
    return count


if __name__ == "__main__":
    day_input = readfile("/Day2/inputday2.txt")
    formatted_input = format_input(day_input)
    part1(formatted_input)
    part2(formatted_input)
