import os


class Slope:
    def __init__(self, strInput):
        self.slope_map = strInput

    def check_coordinate(self, down, across):
        current_row = self.slope_map[down]
        mod_across = across % len(current_row)
        if current_row[mod_across] == "#":
            return True
        else:
            return False


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        lines = f.readlines()
    lines = [line.strip() for line in lines]

    return lines


def part1(strInput, down_increment, across_increment):
    slope = Slope(strInput)
    tree_count = 0
    down = 0
    across = 0
    run = True
    while run:
        down += down_increment
        across += across_increment
        if down >= len(slope.slope_map):
            run = False
            break
        elif slope.check_coordinate(down, across) == True:
            tree_count += 1
    print(tree_count)
    return tree_count


def part2(strInput):
    arg_list = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    ans = 1
    for arg in arg_list:
        ans = ans * part1(strInput, arg[0], arg[1])
    print(ans)
    return ans


if __name__ == "__main__":
    day_input = readfile("/Day3/inputday3.txt")
    part1(day_input, 1, 3)
    part2(day_input)
