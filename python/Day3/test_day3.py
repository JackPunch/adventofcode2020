import pytest
import day3


@pytest.mark.parametrize(
    "strInput, down_increment, across_increment, output",
    [
        (
            [
                "..##.......",
                "#...#...#..",
                ".#....#..#.",
                "..#.#...#.#",
                ".#...##..#.",
                "..#.##.....",
                ".#.#.#....#",
                ".#........#",
                "#.##...#...",
                "#...##....#",
                ".#..#...#.#",
            ],
            1,
            3,
            7,
        )
    ],
)
def test_part1(strInput, down_increment, across_increment, output):
    ans = day3.part1(strInput, down_increment, across_increment)
    assert ans == output


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            [
                "..##.......",
                "#...#...#..",
                ".#....#..#.",
                "..#.#...#.#",
                ".#...##..#.",
                "..#.##.....",
                ".#.#.#....#",
                ".#........#",
                "#.##...#...",
                "#...##....#",
                ".#..#...#.#",
            ],
            336,
        )
    ],
)
def test_part2(strInput, output):
    ans = day3.part2(strInput)
    assert ans == output
