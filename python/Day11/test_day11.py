import pytest
import day10


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL""",
            37,
        )
    ],
)
def test_part1(strInput, output):
    formatted = day10.format_input(strInput)
    ans = day10.part1(formatted)
    assert ans == output


# @pytest.mark.parametrize(
#     "strInput, output",
#     [
#         (
#             """35
# 20
# 15
# 25
# 47
# 40
# 62
# 55
# 65
# 95
# 102
# 117
# 150
# 182
# 127
# 219
# 299
# 277
# 309
# 576""",
#             62,
#         )
#     ],
# )
# def test_part2(strInput, output):
#     formatted = day10.format_input(strInput)
#     ans = day10.part2(formatted, index=5, preamble=5)
#     assert ans == output
