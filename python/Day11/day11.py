import os


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        file_content = f.read()

    return file_content


def format_input(str_input):
    return [i for i in str_input.strip().split("\n")]

    def part1(formatted_input):
        pass


if __name__ == "__main__":
    day_input = readfile("/inputday11.txt")
    # day_input = readfile("/day11/inputday11.txt")
    formatted = format_input(day_input)
    # print(part1(formatted))
