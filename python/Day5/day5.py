import os


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        lines = f.readlines()

    return lines


def part1(seats):
    seat_ids = []
    for seat in seats:
        row = seat.strip()[0:7]
        col = seat.strip()[7:10]
        row_temp = 0
        col_temp = 0
        for index, letter in enumerate(row):
            if letter == "B":
                row_temp += 2 ** (6 - index)
        for index, letter in enumerate(col):
            if letter == "R":
                col_temp += 2 ** (2 - index)
        seat_ids.append((row_temp * 8) + col_temp)

    print(max(seat_ids))
    return max(seat_ids), seat_ids


def part2(seats):
    seat_ids = set(part1(seats)[1])
    all_ids = {i for i in range((0), (127 * 8 + 7))}
    all_ids = all_ids - seat_ids
    all_ids = {id for id in all_ids if 7 < id < (127 * 8)}
    for id in all_ids:
        if (id - 1) in all_ids or (id + 1) in all_ids:
            continue
        else:
            print(id)
            return id


if __name__ == "__main__":
    day_input = readfile("/day5/inputday5.txt")
    part1(day_input)
    part2(day_input)
