import pytest
import day5


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            ["FBFBBFFRLR"],
            357,
        )
    ],
)
def test_part1(strInput, output):

    ans = day5.part1(strInput)[0]
    assert ans == output
