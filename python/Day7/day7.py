import os
import regex as re
from queue import Queue
from collections import defaultdict


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        file_content = f.readlines()

    return file_content


def format_input(file_read):
    bag_dict = defaultdict()
    for bag_line in file_read:
        # bag_line = file_read[0]
        left, right = bag_line.strip().split("contain")
        left_match = re.match(r"^(?<bag_type>\w+ \w+)", left)
        right_match = re.findall(r"(?<amount>\d+) (?<bag_type>\w+ \w+)", right)
        bag_dict[left_match.group("bag_type")] = right_match
    # print(left + "\n" + right)
    # print(left_match.group("bag_type"))
    # print(right_match)
    return bag_dict


def part1(formatted_input):
    success_count = 0
    for key in formatted_input.keys():
        temp_keys = [i[1] for i in formatted_input[key]]
        while len(temp_keys) > 0:
            if "shiny gold" in temp_keys:
                # print(f"Shiny gold bag is in this list of bags:\n{temp_keys}")
                success_count += 1
                break
            temp_key = temp_keys.pop()
            temp_keys.extend([i[1] for i in formatted_input[temp_key]])
    print(success_count)
    return success_count


# def part2(formatted_input):
#     # Calculates the amount fo bags that would be inside the shiny gold bag
#     #First need to edit the intro so it grabs the items for the shiny gold bag and works from there
#     bags_contained = 0
#     temp_keys = [i[1] for i in formatted_input["shiny gold"]]
#     key_value_dict = {i[1]:int(i[0]) for i in formatted_input["shiny gold"]}
#     while len(temp_keys) > 0:
#         temp_key = temp_keys.pop()
#         temp_keys.extend([i[1] for i in formatted_input[temp_key]])
#         bags_contained += key_value_dict[temp_key]
#         key_value_dict.update({i[1]:int(i[0]) * key_value_dict[temp_key] for i in formatted_input[temp_key]})
#         # key_value_dict[temp_key] = 0
#         # key_value_dict.pop(temp_key, None)
#     print(bags_contained)
#     return bags_contained


def part2_format(file_read):
    # Randdalfs slight alteration to formatting
    bag_pattern = re.compile(r"(\d+) (\w+ \w+) bags?")
    rules = {}
    for rule in file_read:
        color, bags = rule.split(" bags contain ")
        rules[color] = {c: int(n) for n, c in bag_pattern.findall(bags)}
    return rules


def part2(rules, target="shiny gold"):
    # Stolen from Randdalf on github
    return sum(n * (1 + part2(rules, color)) for color, n in rules[target].items())


if __name__ == "__main__":
    # day_input = readfile("/inputday7.txt")
    day_input = readfile("/day7/inputday7.txt")
    formatted = format_input(day_input)
    formatted2 = part2_format(day_input)
    part1(formatted)
    print(part2(formatted2))
