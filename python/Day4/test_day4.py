import pytest
import day4


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in""",
            2,
        )
    ],
)
def test_part1(strInput, output):
    formatted_input = day4.format_input(strInput)
    ans = day4.part1(formatted_input)
    assert ans == output


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719""",
            4,
        ),
        (
            """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007""",
            0,
        ),
    ],
)
def test_part2(strInput, output):
    formatted_input = day4.format_input(strInput)
    ans = day4.part2(formatted_input)
    assert ans == output


@pytest.mark.parametrize(
    "f_in, out",
    [
        ({"hgt": "60in"}, True),
        ({"hgt": "190cm"}, True),
        ({"hgt": "190in"}, False),
        ({"hgt": "190"}, False),
    ],
)
def test_check_height(f_in, out):
    assert day4.check_height(f_in) == out


@pytest.mark.parametrize(
    "f_in, out",
    [
        ({"hcl": "#123abc"}, True),
        ({"hcl": "#123abz"}, False),
        ({"hcl": "123abc"}, False),
    ],
)
def test_check_height(f_in, out):
    assert day4.check_hair(f_in) == out


@pytest.mark.parametrize(
    "f_in, out",
    [
        ({"pid": "000000001"}, True),
        ({"pid": "0123456789"}, False),
    ],
)
def test_check_pid(f_in, out):
    assert day4.check_pid(f_in) == out


def test_part2_2():
    day_input = day4.readfile("/Day4/inputday4.txt")
    formatted_input = day4.format_input(day_input)
    assert day4.part2(formatted_input) == 179
