import os
import regex as re


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        line = f.read()

    return line


def format_input(day_input):
    passport_dictionary = []
    all_passports = day_input.split("\n\n")
    for passport in all_passports:
        temp_dict = {}
        fields = passport.strip().split()
        for entry in fields:
            field, value = entry.split(":")
            temp_dict[field] = value
        passport_dictionary.append(temp_dict)

    return passport_dictionary


def part1(passport_list):
    valid_passports = 0
    test_set = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    for passport in passport_list:
        if passport.keys() >= test_set:
            valid_passports += 1
    print(valid_passports)
    return valid_passports


def part2(passport_list):
    valid_passports = 0
    test_set = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    for passport in passport_list:
        if passport.keys() >= test_set:
            if check_passport(passport):
                valid_passports += 1
    print(valid_passports)
    return valid_passports


def check_passport(passport):
    results = []
    results.append(check_dates(passport))
    results.append(check_height(passport))
    results.append(check_hair(passport))
    results.append(check_eyes(passport))
    results.append(check_pid(passport))
    if False in results:
        return False
    else:
        return True


def check_dates(passport):
    byr, iyr, eyr = int(passport["byr"]), int(passport["iyr"]), int(passport["eyr"])
    byr = 1920 <= byr <= 2002
    iyr = 2010 <= iyr <= 2020
    eyr = 2020 <= eyr <= 2030
    if byr and iyr and eyr:
        return True
    return False


def check_height(passport):
    hgt = passport["hgt"]
    hgt_match = re.match(r"(?<value>\d+)(?<unit>cm|in)", hgt)
    if hgt_match and hgt_match.group("unit") == "cm":
        if 150 <= int(hgt_match.group("value")) <= 193:
            return True
    elif hgt_match and hgt_match.group("unit") == "in":
        if 59 <= int(hgt_match.group("value")) <= 76:
            return True
    return False


def check_hair(passport):
    hcl = passport["hcl"]
    if bool(re.match("(#[0-9a-f]{6})", hcl)):
        return True
    return False


def check_eyes(passport):
    ecl = passport["ecl"]
    if bool(re.match("(amb|blu|brn|gry|grn|hzl|oth)", ecl)):
        return True
    return False


def check_pid(passport):
    pid = passport["pid"]
    if bool(re.match(r"^\d{9}$", pid)):
        return True
    return False


if __name__ == "__main__":
    day_input = readfile("/Day4/inputday4.txt")
    formatted_input = format_input(day_input)
    part1(formatted_input)
    part2(formatted_input)
