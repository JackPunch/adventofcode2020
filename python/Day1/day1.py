import os


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        lines = f.readlines()

    lines = [int(item.strip()) for item in lines]

    return lines


def part1(dayinput):
    for x in dayinput:
        for y in dayinput:
            if x + y == 2020:
                print(x * y)
                return x * y


def part2(dayinput):
    for x in dayinput:
        for y in dayinput:
            for z in dayinput:
                if x + y + z == 2020:
                    print(x * y * z)
                    return x * y * z


if __name__ == "__main__":
    dayinput = readfile("/Day1/inputday1.txt")
    part1(dayinput)
