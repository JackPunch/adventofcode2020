import pytest
import day1


@pytest.mark.parametrize(
    "intInput, output", [([1721, 979, 366, 299, 675, 1456], 514579)]
)
def test_part1(intInput, output):
    ans = day1.part1(intInput)
    assert ans == output


@pytest.mark.parametrize(
    "intInput, output", [([1721, 979, 366, 299, 675, 1456], 241861950)]
)
def test_part2(intInput, output):
    ans = day1.part2(intInput)
    assert ans == output
