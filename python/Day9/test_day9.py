import pytest
import day9


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576""",
            127,
        )
    ],
)
def test_part1(strInput, output):
    formatted = day9.format_input(strInput)
    ans = day9.part1(formatted, index=5, preamble=5)
    assert ans == output


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576""",
            62,
        )
    ],
)
def test_part2(strInput, output):
    formatted = day9.format_input(strInput)
    ans = day9.part2(formatted, index=5, preamble=5)
    assert ans == output
