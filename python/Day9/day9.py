import os
import math
from itertools import combinations


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        file_content = f.read()

    return file_content


def format_input(str_input):
    return [int(i) for i in str_input.strip().split("\n")]


def part1(formatted_input, index=25, preamble=25):
    # Starts at index 25 after the preamble
    previous = formatted_input[index - preamble : index]
    previous_sums = [sum(i) for i in list(combinations(previous, 2))]
    # print(previous_sums, " ###### ", formatted_input[index])
    if formatted_input[index] not in previous_sums:
        return formatted_input[index]
    elif index + 1 < len(formatted_input):
        return part1(formatted_input, index + 1)
    else:
        print("Reached end of input")


def part2(formatted_input, index=25, preamble=25):
    part1_result = part1(formatted_input, index, preamble)
    combination_list = part2_contiguous(formatted_input, part1_result, 2)
    print(combination_list)
    answer = min(combination_list) + max(combination_list)
    return answer
    # Basically use combinations, increasing the second argument until the value
    # comes out then use the index of that value to get the index of the list that
    # created it and then add the min and max together


# This doesn't work for contiguous sets of number
# def part2_recur(formatted_input, target, r_length):
#     list_combinations = list(combinations(formatted_input, r_length))
#     list_sums = [sum(i) for i in list_combinations]
#     if target in list_sums:
#         return list_combinations[list_sums.index(target)]
#     return part2_recur(formatted_input, target, r_length+1)


def part2_contiguous(formatted_input, target, sample_length):
    # I nearly stole Randdalf's solution as it has some really intelligent deductions
    # But opted to hacking my own budget solution
    list_combinations = [
        formatted_input[i : i + sample_length]
        for i in range(len(formatted_input) - sample_length)
    ]
    list_sums = [sum(i) for i in list_combinations]
    if target in list_sums:
        return list_combinations[list_sums.index(target)]
    return part2_contiguous(formatted_input, target, sample_length + 1)


if __name__ == "__main__":
    # day_input = readfile("/inputday9.txt")
    day_input = readfile("/day9/inputday9.txt")
    formatted = format_input(day_input)
    print(part1(formatted))
    print(part2(formatted))
