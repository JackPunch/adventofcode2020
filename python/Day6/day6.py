# %%
import os
from collections import defaultdict


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        file_content = f.read()

    return file_content


def format_input(file_content):
    formatted = []
    groups = file_content.split("\n\n")
    for group in groups:
        formatted.append(group.split("\n"))
    return formatted


def part1(groups_list):
    answer_count = 0
    temp_set = set()
    for group in groups_list:
        for answer in group:
            temp = [i for i in answer]
            temp_set.update(temp)
        answer_count += len(temp_set)
        temp_set.clear()
    print(answer_count)


def part2(groups_list):
    answer_count = 0
    for group in groups_list:
        # Perform an and operation from the start
        temp_set = set([i for i in group[0]])
        for answer in group:
            temp = [i for i in answer]  # Perform a and operation from the start
            if len(temp) > 0:
                temp_set = temp_set.intersection(temp)
        answer_count += len(temp_set)
    print(answer_count)


if __name__ == "__main__":
    day_input = readfile("/day6/inputday6.txt")
    formatted = format_input(day_input)
    part1(formatted)
    part2(formatted)

# %%
