import pytest
import day8


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6""",
            5,
        )
    ],
)
def test_part1(strInput, output):
    gc = day8.GameConsole(strInput)
    ans = gc.start()
    assert ans == output


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6""",
            "nop",
        ),
        (
            """nop +0
jmp +4
acc +1
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6""",
            "acc",
        ),
    ],
)
def test_nop_jmp_flip(strInput, output):
    """
    Just tests the 3rd element to make sure it does and doesn't change value
    input is corrected to test different functionality
    """
    gc = day8.GameConsole(strInput)
    mode_before, arg_before = gc.boot_code.get_line(2)
    gc.boot_code.nop_jmp_flip(2)
    mode_after, arg_after = gc.boot_code.get_line(2)
    assert mode_after == output
    assert arg_before == arg_after


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6""",
            8,
        )
    ],
)
def test_part2(strInput, output):
    ans = day8.part2(strInput)
    assert ans == output
