import os
import regex as re
from queue import Queue
from collections import defaultdict


class GameConsole:
    def __init__(self, boot_code):
        self.index = 0
        self.accumulator = 0
        self.index_history = set()
        self.boot_code = BootCode(boot_code)

    def mode(self, mode_str, arg):
        modes = {"nop": self.nop, "jmp": self.jmp, "acc": self.acc}
        return modes[mode_str](arg) if mode_str in modes else print("Unrecognised mode")

    def nop(self, nop_arg):
        self.index += 1
        return self.next()

    def jmp(self, jmp_arg):
        self.index += jmp_arg
        return self.next()

    def acc(self, acc_arg):
        self.accumulator += acc_arg
        self.index += 1
        return self.next()

    def end(self):
        return (self.index, self.accumulator)

    def next(self):
        if self.index in self.index_history:
            # print(self.accumulator)
            return self.accumulator
        elif self.index == len(self.boot_code.boot_code):
            return {"status": "Ended correctly", "accumulator": self.accumulator}
        else:
            self.index_history.add(self.index)
            mode, arg = self.boot_code.get_line(self.index)
            return self.mode(mode, arg)

    def start(self):
        return self.next()


class BootCode:
    def __init__(self, boot_code):
        self.index = 0
        self.boot_code = boot_code
        self.format_input()

    def format_input(self):
        lines = self.boot_code.strip().split("\n")
        reformatted = [(i.split()[0], int(i.split()[1])) for i in lines]
        self.boot_code = reformatted

    def get_line(self, index):
        return self.boot_code[index]

    def set_line(self, index, value):
        self.boot_code[index] = value

    def nop_jmp_flip(self, index):
        temp_line = self.get_line(index)
        if "jmp" in temp_line:
            self.set_line(index, ["nop", temp_line[1]])
        elif "nop" in temp_line:
            self.set_line(index, ["jmp", temp_line[1]])
        # else:
        #     print(f"Index: {index}, must be a jmp or nop argument but is instead \n{temp_line}")


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        file_content = f.read()

    return file_content


def part2(boot_code, index=0):
    gc = GameConsole(boot_code)
    gc.boot_code.nop_jmp_flip(index)
    result = gc.start()
    if type(result) == dict:
        print(result["accumulator"])
        return result["accumulator"]
    return part2(boot_code, index + 1)


if __name__ == "__main__":
    # day_input = readfile("/inputday8.txt")
    day_input = readfile("/day8/inputday8.txt")
    gc = GameConsole(day_input)
    gc.start()
    part2(day_input)
