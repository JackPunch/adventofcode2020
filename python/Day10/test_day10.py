import pytest
import day10


@pytest.mark.parametrize(
    "strInput, output",
    [
        (
            """16
10
15
5
1
11
7
19
6
12
4""",
            35,
        ),
        (
            """28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3""",
            220,
        ),
    ],
)
def test_part1(strInput, output):
    formatted = day10.format_input(strInput)
    ans = day10.part1(formatted)
    assert ans == output


# @pytest.mark.parametrize(
#     "strInput, output",
#     [
#         (
#             """35
# 20
# 15
# 25
# 47
# 40
# 62
# 55
# 65
# 95
# 102
# 117
# 150
# 182
# 127
# 219
# 299
# 277
# 309
# 576""",
#             62,
#         )
#     ],
# )
# def test_part2(strInput, output):
#     formatted = day10.format_input(strInput)
#     ans = day10.part2(formatted, index=5, preamble=5)
#     assert ans == output
