import os
from itertools import combinations


def readfile(relpath):
    path = os.getcwd() + relpath
    with open(path, "r") as f:
        file_content = f.read()

    return file_content


def format_input(str_input):
    return [int(i) for i in str_input.strip().split("\n")]


def part1(formatted_input):
    diff_count = {1: 0, 2: 0, 3: 0}
    formatted_input.extend([0, max(formatted_input) + 3])
    sorted_input = sorted(formatted_input)
    print(sorted_input)
    for i in range(len(formatted_input) - 1):
        diff_count[sorted_input[i + 1] - sorted_input[i]] += 1
    print(diff_count)
    return diff_count[1] * diff_count[3]


if __name__ == "__main__":
    day_input = readfile("/inputday10.txt")
    # day_input = readfile("/day10/inputday10.txt")
    formatted = format_input(day_input)
    print(part1(formatted))
