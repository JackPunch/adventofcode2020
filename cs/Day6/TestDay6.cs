using System;
using Xunit;
using System.Collections.Generic;
using Day6;

public class TestDay6
{
    [Fact]
    public void TestingPart1()
    {
        string dayInput = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb";
        List<string> formattedinput = Program.FormatInput(dayInput);
        Assert.Equal(11, Program.Part1(formattedinput));
    }
    [Fact]
    public void TestingPart2_1()
    {
        string dayInput = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb";
        List<Tuple<string, int>> formattedinput = Program.FormatInput2(dayInput);
        Assert.Equal(6, Program.Part2(formattedinput));
    }
    [Fact]
    public void TestingPart2_2()
    {
        string dayInput = "absdepjhctyfzxnivom\nfdvbjnsolpztgywmaihx\ntkashzxmjbydivfnrop\nptnsojahvxmbdzfiy";
        List<Tuple<string, int>> formattedinput = Program.FormatInput2(dayInput);
        int ans = Program.Part2(formattedinput);
        Assert.Equal(17, ans);
    }
}