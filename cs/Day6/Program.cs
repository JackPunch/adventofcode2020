﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Utilities;

namespace Day6
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday6.txt"); //Sets path to input
            string dayInput = RF.GetString(); //Reads input
            // string dayInput = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb";
            List<string> formattedInput1 = FormatInput(dayInput);
            List<Tuple<string, int>> formattedInput2 = FormatInput2(dayInput);
            Part1(formattedInput1);
            Part2(formattedInput2);
        }
        public static List<string> FormatInput(string strInput)
        {
            string[] formattedInput = strInput.Split("\n\n");
            IEnumerable<string> strippedInput = from line in formattedInput select line.Replace("\n", "");
            return strippedInput.ToList();
        }
        public static List<Tuple<string, int>> FormatInput2(string strInput)
        {
            List<Tuple<string, int>> tupleList = new List<Tuple<string, int>>();
            string[] splitInput = strInput.Split("\n\n");
            foreach (string questionSection in splitInput)
            {
                string searchString = "\n";
                int searchCount = 1 + (questionSection.Length - questionSection.Replace(searchString, "").Length) / searchString.Length;
                tupleList.Add(new Tuple<string, int>(questionSection.Replace("\n", ""), searchCount));
            }
            return tupleList;
        }
        public static int Part1(List<string> strInput)
        {
            var hashSet = new HashSet<char>();
            int count = 0;
            foreach (string answers in strInput)
            {
                char[] answer = answers.ToCharArray();
                foreach (char letter in answer)
                {
                    hashSet.Add(letter);
                }
                count += hashSet.Count;
                hashSet.Clear();
            }
            System.Console.WriteLine(count);
            return count;
        }
        public static int Part2(List<Tuple<string, int>> tupleInput)
        {
            var hashSet = new HashSet<char>();
            int count = 0;
            foreach (Tuple<string, int> answers in tupleInput)
            {
                char[] answer = answers.Item1.ToCharArray();
                foreach (char letter in answer)
                {
                    hashSet.Add(letter);
                }
                foreach (char letter in hashSet)
                {
                    int letterCount = answer.Count(s => s == letter);
                    if (letterCount == answers.Item2)
                    {
                        count += 1;
                    }
                }
                hashSet.Clear();
            }
            System.Console.WriteLine(count);
            return count;
        }
    }
}
