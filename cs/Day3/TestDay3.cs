using Xunit;
using System.Collections.Generic;
using Day3;

public class TestDay3
{
    [Fact]
    public void TestingPart1()
    {
        string[] dayInput = new string[] {
            "..##.......", "#...#...#..", ".#....#..#.", "..#.#...#.#",
            ".#...##..#.", "..#.##.....", ".#.#.#....#", ".#........#",
            "#.##...#...", "#...##....#", ".#..#...#.#"
         };
        Assert.Equal(7, Program.Part1(dayInput, 1, 3));
    }

    [Fact]
    public void TestingPart2()
    {
        string[] dayInput = new string[] {
            "..##.......", "#...#...#..", ".#....#..#.", "..#.#...#.#",
            ".#...##..#.", "..#.##.....", ".#.#.#....#", ".#........#",
            "#.##...#...", "#...##....#", ".#..#...#.#"
         };
        Assert.Equal(336, Program.Part2(dayInput));
    }
}