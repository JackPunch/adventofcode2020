﻿using System;
using System.IO;
using System.Collections.Generic;
using Utilities;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday3.txt"); //Sets path to input
            string[] dayInput = RF.GetLines(); //Reads input
            Part1(dayInput, 1, 3);
            Part2(dayInput);
        }

        public static int Part1(string[] strInput, int downIncrease, int acrossIncrease)
        {
            bool run = true;
            Slope skiSlope = new Slope(strInput);
            int down = 0;
            int across = 0;
            int count = 0;
            while (run == true)
            {
                down += downIncrease;
                across += acrossIncrease;
                if (skiSlope.slopeMap.Length <= down)
                {
                    run = false;
                    break;
                }
                if (skiSlope.CheckCoordinate(down, across) == true)
                {
                    count += 1;
                }

            }
            System.Console.WriteLine(count);
            return count;
        }

        public static int Part2(string[] strInput)
        {
            Tuple<int, int>[] paths = {
                new Tuple<int, int>(1, 3), new Tuple<int, int>(1, 1),
                new Tuple<int, int>(1, 5), new Tuple<int, int>(1, 7), new Tuple<int, int>(2, 1)
                };
            List<int> treesPerPath = new List<int>();
            int ans = 1;
            foreach (var path in paths)
            {
                treesPerPath.Add(Part1(strInput, path.Item1, path.Item2));
            }
            foreach (int treeCount in treesPerPath)
            {
                ans = ans * treeCount;
            }
            System.Console.WriteLine(ans);
            return ans;
        }
    }
    public class Slope
    {
        public string[] slopeMap;
        public Slope(string[] strInput)
        {
            slopeMap = strInput;
        }
        public bool CheckCoordinate(int downhill, int acrosshill)
        {
            string currentRow = slopeMap[downhill];
            int currentColumnWidth = currentRow.Length;
            int currentColumnPosition = acrosshill % currentColumnWidth;
            if (currentRow[currentColumnPosition] == char.Parse("#"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
