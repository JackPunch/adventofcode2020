using System;
using System.IO;

namespace Utilities
{
    public class ReadFile
    {
        public string FilePath { get; set; }

        public string[] GetLines()
        {
            string[] lines = File.ReadAllLines(FilePath);
            return lines;
        }

        public string GetString()
        {
            string fileText = File.ReadAllText(FilePath);
            return fileText;
        }
    }
}