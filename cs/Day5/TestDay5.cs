using Xunit;
using System.Collections.Generic;
using Day5;

public class TestDay5
{
    [Fact]
    public void TestingPart1_1()
    {
        string[] dayInput = new string[]
        {
            "BFFFBBFRRR"
        };
        Assert.Equal(567, Program.Part1(dayInput));
    }
    [Fact]
    public void TestingPart1_2()
    {
        string[] dayInput = new string[]
        {
            "FFFBBBFRRR"
        };
        Assert.Equal(119, Program.Part1(dayInput));
    }
    [Fact]
    public void TestingPart1_3()
    {
        string[] dayInput = new string[]
        {
            "BBFFBBFRLL"
        };
        Assert.Equal(820, Program.Part1(dayInput));
    }
}