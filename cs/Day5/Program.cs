﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Utilities;

namespace Day5
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday5.txt"); //Sets path to input
            string[] dayInput = RF.GetLines(); //Reads input
            Part1(dayInput);
            Part2(dayInput);
        }
        public static int Part1(string[] strInput)
        {
            List<int> ids = new List<int>();
            foreach (string item in strInput)
            {
                ids.Add(Parse(item));
            }
            System.Console.WriteLine(ids.Max());
            return ids.Max();

        }
        public static int Part2(string[] strInput)
        {
            // list2.Except(list1).ToList()
            List<int> ids = new List<int>();
            List<int> idSequence = new List<int>();
            foreach (string item in strInput)
            {
                ids.Add(Parse(item));
            }
            for (int i = 0; i < 128; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    idSequence.Add((i * 8) + j);
                }
            }
            List<int> missingID = idSequence.Except(ids).ToList();
            foreach (int seatID in missingID)
            {
                bool above = missingID.Contains(seatID + 1);
                bool below = missingID.Contains(seatID - 1);
                if (seatID > 8 && seatID < 1016 && above == false && below == false)
                {
                    System.Console.WriteLine(seatID);
                    return seatID;
                }
            }
            return 0;
        }
        public static int Parse(string strInput)
        {
            string row = strInput.Substring(0, 7);
            string column = strInput.Substring(7, 3);
            double rn = 0;
            double cn = 0;
            for (int i = 0; i < row.Length; i++)
            {
                if (row[i] == 'B')
                {
                    rn += Math.Pow(2, (6 - i));
                }
            }
            for (int i = 0; i < column.Length; i++)
            {
                if (column[i] == 'R')
                {
                    cn += Math.Pow(2, (2 - i));
                }
            }
            return Convert.ToInt32((rn * 8) + cn);
        }
    }
}
