﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Utilities;

namespace Day7
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday7.txt"); //Sets path to input
            string[] dayInput = RF.GetLines(); //Reads input
            Part1(dayInput);
        }

        public static int Part1(string[] strInput)
        {
            int passCount = 0;
            Dictionary<string, BagRecipe> recipeDictionary = new Dictionary<string, BagRecipe>();
            List<BagRecipe> allBags = new List<BagRecipe>();
            foreach (string recipe in strInput)
            {
                BagRecipe currentRecipe = new BagRecipe(recipe);
                allBags.Add(currentRecipe);
                recipeDictionary.Add(currentRecipe.outerBag, currentRecipe);
            }
            foreach (BagRecipe bag in allBags)
            {
                if (CheckBag(bag, recipeDictionary))
                {
                    passCount += 1;
                }
            }
            return passCount;
        }
        public static bool CheckBag(BagRecipe bag, Dictionary<string, BagRecipe> recipeDictionary)
        {
            List<BagRecipe> usedRecipes = new List<BagRecipe>();
            Queue<string> bagsUsed = bag.GetInnerBags();
            while (bagsUsed.Count > 0)
            {
                string bagName = bagsUsed.Dequeue();
                if (bagName == "shiny gold")
                {
                    return true;
                }
                else
                {
                    Queue<string> bagsAdded = recipeDictionary[bagName].GetInnerBags();
                    while (bagsAdded.Count > 0)
                    {
                        // System.Console.WriteLine($"Bag to be added - {bagsAdded.Peek()}");
                        bagsUsed.Enqueue(bagsAdded.Dequeue());
                    }
                }
            }
            return false;
        }
    }
    class BagRecipe
    {
        public string outerBag;
        public List<Tuple<int, string>> innerBags = new List<Tuple<int, string>>();

        public BagRecipe(string strInput)
        {
            Regex r1 = new Regex(@"(?<bag>\w+ \w+) bags contain");
            Regex r2 = new Regex(@"(?<amount>\d+) (?<bag>\w+ \w+) bags?\b");
            Match outer = r1.Match(strInput);
            MatchCollection inner = r2.Matches(strInput);
            SetVariables(outer, inner);
        }

        public void SetVariables(Match outer, MatchCollection inner)
        {
            this.outerBag = outer.Groups["bag"].Value;
            foreach (Match bag in inner)
            {
                int value = Int32.Parse(bag.Groups["amount"].Value);
                string type = bag.Groups["bag"].Value;
                innerBags.Add(new Tuple<int, string>(value, type));
            }
        }
        public Queue<string> GetInnerBags()
        {
            Queue<string> innerBagTypes = new Queue<string>();
            foreach (Tuple<int, string> bag in this.innerBags)
            {
                innerBagTypes.Enqueue(bag.Item2);
            }
            return innerBagTypes;
        }
    }
}
