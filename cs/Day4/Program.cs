﻿using System;
using System.IO;
using System.Collections.Generic;
using Utilities;
using System.Text.RegularExpressions;

namespace Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday4.txt"); //Sets path to input
            string[] dayInput = RF.GetLines(); //Reads input
            List<string> formattedInput = FormatInput(dayInput);
            Part1(formattedInput);
            Part2(formattedInput);
        }

        public static List<string> FormatInput(string[] strInput)
        {
            List<string> passportCollection = new List<string>();
            string buildString = "";
            foreach (string line in strInput)
            {
                if (line.Length == 0)
                {
                    passportCollection.Add(buildString.Trim());
                    buildString = "";
                }
                else
                {
                    buildString = buildString + " " + line;
                }
            }
            passportCollection.Add(buildString.Trim());
            return passportCollection;
        }

        static bool CheckSubstrings(string[] checkStrings, string stringChecked)
        {
            foreach (string check in checkStrings)
            {
                if (stringChecked.Contains(check))
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public static int Part1(List<string> passports)
        {
            int count = 0;
            string[] requiredFields = new string[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
            foreach (string passport in passports)
            {
                if (CheckSubstrings(requiredFields, passport))
                {
                    count += 1;
                }
            }
            System.Console.WriteLine(count);
            return count;
        }
        public static int Part2(List<string> passports)
        {
            List<Passport> passportCollection = new List<Passport>();
            int count = 0;
            string[] requiredFields = new string[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
            foreach (string passport in passports)
            {
                if (CheckSubstrings(requiredFields, passport))
                {
                    passportCollection.Add(new Passport(passport));
                }
            }
            foreach (Passport item in passportCollection)
            {
                if (item.passedChecks)
                {
                    count += 1;
                }
            }
            System.Console.WriteLine(count);
            return count;
        }
    }
    public class Passport
    {
        Dictionary<string, string> passportDict = new Dictionary<string, string>();
        public bool passedChecks;
        public Passport(string strInput)
        {
            FormatInput(strInput);
            passedChecks = false;
            PassportChecks();
        }
        private void FormatInput(string strInput)
        {
            string[] firstSplit = strInput.Split(" ");
            foreach (string item in firstSplit)
            {
                string[] valuePair = item.Split(":");
                passportDict.Add(valuePair[0], valuePair[1]);
            }
        }
        private void PassportChecks()
        {
            List<bool> results = new List<bool> { FirstCheck(), HeightCheck(), HairCheck(), EyeCheck(), PidCheck() };
            if (!results.Contains(false))
            {
                passedChecks = true;
            }
        }

        private bool FirstCheck()
        {
            try
            {
                int byr = Int32.Parse(passportDict["byr"]);
                int iyr = Int32.Parse(passportDict["iyr"]);
                int eyr = Int32.Parse(passportDict["eyr"]);
                if (1920 <= byr && byr <= 2002 && 2010 <= iyr && iyr <= 2020 && 2020 <= eyr && eyr <= 2030)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (System.Exception)
            {

                return false;
            }

        }
        private bool HeightCheck()
        {
            string pattern = @"(?<value>\d+)(?<unit>cm|in)";
            Regex rx = new Regex(pattern);
            bool rxBool = rx.IsMatch(passportDict["hgt"]);
            if (rxBool)
            {
                Match rxMatch = rx.Match(passportDict["hgt"]);
                int value = Int32.Parse(rxMatch.Groups["value"].Value);
                string unit = rxMatch.Groups["unit"].Value;
                if (unit == "cm" && value >= 150 && 193 >= value)
                {
                    return true;
                }
                else if (unit == "in" && value >= 59 && 76 >= value)
                {
                    return true;
                }
            }
            return false;
        }
        private bool HairCheck()
        {
            string pattern = @"(#[0-9a-f]{6})";
            Regex rx = new Regex(pattern);
            bool rxBool = rx.IsMatch(passportDict["hcl"]);
            if (rxBool)
            {
                return true;
            }
            return false;
        }
        private bool EyeCheck()
        {
            string pattern = @"(amb|blu|brn|gry|grn|hzl|oth)";
            Regex rx = new Regex(pattern);
            bool rxBool = rx.IsMatch(passportDict["ecl"]);
            if (rxBool)
            {
                return true;
            }
            return false;
        }
        private bool PidCheck()
        {
            string pattern = @"^(\d{9})$";
            Regex rx = new Regex(pattern);
            bool rxBool = rx.IsMatch(passportDict["pid"]);
            if (rxBool)
            {
                return true;
            }
            return false;
        }
    }
}