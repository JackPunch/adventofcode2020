﻿using System;
using System.IO;
using System.Collections.Generic;
using Utilities;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday1.txt"); //Sets path to input
            string[] dayInput = RF.GetLines(); //Reads input
            List<int> intInput = FormatInput(dayInput); //Formats input

            Part1(intInput);
            Part2(intInput);
        }

        private static List<int> FormatInput(string[] stringInput)
        {
            List<int> intInput = new List<int>();
            foreach (string line in stringInput)
            {
                intInput.Add(Int32.Parse(line));
            }
            return intInput;
        }
        public static int Part1(List<int> intInput)
        {
            foreach (int x in intInput)
            {
                foreach (int y in intInput)
                {
                    if (x + y == 2020)
                    {
                        System.Console.WriteLine(x * y);
                        return x * y;
                    }
                }
            }
            return 0;
        }
        public static int Part2(List<int> intInput)
        {
            foreach (int x in intInput)
            {
                foreach (int y in intInput)
                {
                    foreach (int z in intInput)
                    {
                        if (x + y + z == 2020)
                        {
                            System.Console.WriteLine(x * y * z);
                            return x * y * z;
                        }
                    }
                }
            }
            return 0;
        }
    }
}
