using Xunit;
using System.Collections.Generic;
using Day1;

public class TestDay1
{
    [Fact]
    public void TestingPart1()
    {
        List<int> dayInput = new List<int>() { 1721, 979, 366, 299, 675, 1456 };
        Assert.Equal(514579, Program.Part1(dayInput));
    }

    [Fact]
    public void TestingPart2()
    {
        List<int> dayInput = new List<int>() { 1721, 979, 366, 299, 675, 1456 };
        Assert.Equal(241861950, Program.Part2(dayInput));
    }
}