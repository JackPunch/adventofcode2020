using Xunit;
using System.Collections.Generic;
using Day2;

public class TestDay2
{
    [Fact]
    public void TestingPart1()
    {
        string[] dayInput = new string[] { "1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc" };
        List<PasswordConditions> PC = Program.FormatInput(dayInput);
        Assert.Equal(2, Program.Part1(PC));
    }

    [Fact]
    public void TestingPart2()
    {
        string[] dayInput = new string[] { "1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc" };
        List<PasswordConditions> PC = Program.FormatInput(dayInput);
        Assert.Equal(1, Program.Part2(PC));
    }
}