﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Utilities;
using System.Text.RegularExpressions;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile RF = new ReadFile(); //Initialises ReadFile class
            RF.FilePath = Path.Combine(Directory.GetCurrentDirectory(), "inputday2.txt"); //Sets path to input
            string[] dayInput = RF.GetLines(); //Reads input

            List<PasswordConditions> PC = FormatInput(dayInput);
            Part1(PC);
            Part2(PC);
        }

        public static List<PasswordConditions> FormatInput(string[] dayInput)
        {
            List<PasswordConditions> conditionsList = new List<PasswordConditions>();
            foreach (var conditions in dayInput)
            {
                string pattern = @"\b([1-9][0-9]?)+-([1-9][0-9]?)+\s+([a-z])+:+\s+(\w+)";
                Regex rx = new Regex(pattern);
                Match m = rx.Match(conditions);
                conditionsList.Add(new PasswordConditions()
                {
                    password = m.Groups[4].Value,
                    letter = Convert.ToChar(m.Groups[3].Value),
                    maxCount = Int32.Parse(m.Groups[2].Value),
                    minCount = Int32.Parse(m.Groups[1].Value)
                });
            }
            return conditionsList;
        }

        public static int Part1(List<PasswordConditions> allPasswords)
        {
            int validCount = 0;
            foreach (var password in allPasswords)
            {
                var letterCount = password.password.Count(x => x == password.letter);
                if (letterCount <= password.maxCount && letterCount >= password.minCount)
                {
                    validCount += 1;
                }
            }
            System.Console.WriteLine(validCount);
            return validCount;
        }

        public static int Part2(List<PasswordConditions> allPasswords)
        {
            int validCount = 0;
            foreach (var password in allPasswords)
            {
                if (
                    password.password[password.maxCount - 1] == password.letter ^
                    password.password[password.minCount - 1] == password.letter
                    )
                {
                    validCount += 1;
                }
            }
            System.Console.WriteLine(validCount);
            return validCount;
        }
    }
    public class PasswordConditions
    {
        public string password { get; set; }
        public char letter { get; set; }
        public int minCount { get; set; }
        public int maxCount { get; set; }
    }
}
